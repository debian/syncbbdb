#------------------------------------------------------------------------------
#   $Date: 2005/09/11 14:48:36 $
#   RCS: $Id: ConduitHost.pm,v 1.5 2005/09/11 14:48:36 aaronkaplan Exp $
#------------------------------------------------------------------------------

package ConduitHost;

use strict;
use PDA::Pilot;
use Carp;

my @plversion;          # pilot-link version (version, major, minor, patch)

# any or alll of these may be undefined, depending on the
# pilot-link version.
eval {
    $plversion[0] = PDA::Pilot::PILOT_LINK_VERSION();
    $plversion[1] = PDA::Pilot::PILOT_LINK_MAJOR();
    $plversion[2] = PDA::Pilot::PILOT_LINK_MINOR();
    $plversion[3] = PDA::Pilot::PILOT_LINK_PATCH();
};

sub new {
  my $type = shift;
  my $self = {};

  bless ($self, $type);

  $self->{'prefix'} = "ATTN: ";

  $self;
}

sub finish {
}

sub output {
  my $self = shift;
  my $str  = shift;
  print STDOUT $self->{'prefix'} . $str;
}

sub status {
    my $self = shift;
    my $msg  = shift;
    my $done = shift;
    print STDOUT "$msg, completed: $done%\n";
}

# Hopefully this is temporary, until the pilot-link perl bindings
# provide a more convenient way of checking.  Argument is a
# PDA::Pilot::DLP or a PDA::Pilot::DLP::DB.
sub checkErrNotFound
{
    my($obj) = @_;
    my $errno = $obj->errno();
    if (defined $plversion[0]) { # version is >= 0.12.0-pre2
        if ($errno != PDA::Pilot::PI_ERR_DLP_PALMOS()) {
            croak "Error $errno";
        }
        if (($errno = $obj->palmos_errno()) != PDA::Pilot::dlpErrNotFound()) {
            croak "Error $errno: " . errorText($errno);
        }
    } else {
        croak "Error $errno" if ($errno != -5);  # dlpErrNotFound
    }
}

# This should be called every now and then so the host can deal with
# anything that needs to be done (tickling the Pilot, updating the UI
# you get the idea..
sub update {
}

1;

