#------------------------------------------------------------------------------
#   $Date: 2004/12/15 19:39:21 $
#   RCS: $Id: PilotMgrHost.pm,v 1.2 2004/12/15 19:39:21 aaronkaplan Exp $
#------------------------------------------------------------------------------

package PilotMgrHost;

require 5.000;
use strict;
use SyncUtil::ConduitHost;
use FileHandle;

my @ISA = qw(ConduitHost);

sub new {
  my $type = shift;

  my $self = ConduitHost->new();

  bless($self, $type);

  $self->{'name'} = shift;
  $self->{'dlp'}  = shift;
  $self->{'info'} = shift;

  $self->{'last status msg'}     = "";
  $self->{'last status percent'} = 0;
  $self->{'last status time'}    = 0;
  
  return $self;
}

sub init {
}

sub finish {
}

sub output {
  my $self = shift;
  my $str  = shift;
  PilotMgr::msg($self->{'prefix'} . $str);
}

sub status {
    my $self = shift;
    my $msg  = shift;
    my $done = shift;

    my $ctime = time;
    # Every 1 sec or 5 percent...
    if (($self->{'last status msg'}      ne $msg)   ||
	($self->{'last status percent'}+5 < $done)  ||
	($self->{'last status time'}+1    < $ctime)) {

      PilotMgr::status($msg, $done);
	
	$self->{'last status msg'}     = $msg;
	$self->{'last status percent'} = $done;
	$self->{'last status time'}    = $ctime;
    }
}

sub getUserID {
  my $self = shift;
  return $self->{'info'}->{'userID'};
}

sub getUserName {
  my $self = shift;
  return $self->{'info'}->{'name'};
}

sub getConduitDir {
  my $self = shift;
  return $self->{'name'};
}

sub update {
  my $self = shift;
  $self->{'dlp'}->tickle();
}
