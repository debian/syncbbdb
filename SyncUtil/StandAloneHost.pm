#------------------------------------------------------------------------------
#   $Date: 2004/12/15 20:22:22 $
#   RCS: $Id: StandAloneHost.pm,v 1.3 2004/12/15 20:22:22 aaronkaplan Exp $
#------------------------------------------------------------------------------

package StandAloneHost;

require 5.000;
use strict;
use SyncUtil::ConduitHost;
use FileHandle;

my @ISA = qw(ConduitHost);

sub new {
  my $type = shift;

  my $self = ConduitHost->new();

  $self->{'name'}  = shift;
  $self->{'port'}  = shift;
  $self->{'prefs'} = shift;
  
  bless($self, $type);

  $self->{'last status msg'}     = "";
  $self->{'last status percent'} = 0;
  $self->{'last status time'}    = 0;

  if (!defined($self->{'port'})) {
    $self->{'port'} = "/dev/ttyb";
  }

  my $conduitdir = $self->{'name'};
  if ($conduitdir !~ m/"^\."/) {
    $conduitdir = ".$conduitdir";
  }
  $self->{'conduitdir'} = $ENV{'HOME'} . "/" . $conduitdir;

  if (! defined $self->{'prefs'}) {

    $self->{'prefs'} =  $self->{'conduitdir'} . "/prefs";
  }

  ## Load Pilot Hash's this lets us know if a pilot entry is new or if
  ## the record was deleted from BBDB.  Also in the case of a full
  ## sync it lets us decide if the record has been modified.
  my $prefs = new FileHandle($self->{'prefs'});
  
  if ($prefs)
    {
      $self->output("Prefs: $self->{'prefs'}\n");
      chop($self->{'pcid'}     = <$prefs>);
      chop($self->{'syncTime'} = <$prefs>);
    }
  else
    {
      $self->{'pcid'}     = int(rand(2147483648) + 1);
      $self->{'syncTime'} = 0;
    }
  $prefs = undef;

  $self->output("Please start HotSync now.\n");

  $self->{'psocket'} = PDA::Pilot::openPort($self->{'port'});
  die "Unable to open Pilot port $self->{'port'}\n" if (!$self->{'psocket'});

  $self->{'dlp'} = PDA::Pilot::accept($self->{'psocket'});
  die "Cancelled.\n" if (!$self->{'dlp'} || 
			 $self->{'dlp'}->getStatus()<0);

  $self->{'info'} = $self->{'dlp'}->getUserInfo();
  $self->{'info'}->{'thisSyncDate'} = time;

  # PC Id's don't match so we can't do a fast sync...
  if ($self->{'info'}->{'lastSyncPC'} ne $self->{'pcid'}) {
      $self->{'info'}->{'lastSyncDate'} = 0;
      $self->{'info'}->{'successfulSyncDate'} = 0;
  }

  return $self;
}

sub finish {
  my $self = shift;

  $self->{'syncTime'}               = $self->{'info'}->{'thisSyncDate'};
  $self->{'info'}->{'lastSyncDate'} = $self->{'info'}->{'thisSyncDate'};
  $self->{'info'}->{'lastSyncPC'}   = $self->{'pcid'};

  $self->{'dlp'}->setUserInfo($self->{'info'});
  $self->{'dlp'}->close();

  my $prefs = new FileHandle(">$self->{'prefs'}");
  if (! $prefs) {
      $self->{'prefs'} =~ m|(.*?)[^/]*$|;
      my $dir = $1;
      if ((! -d $dir) && (! mkdir $dir)) {
	  die "Couldn't create $dir";
      }
      $prefs = new FileHandle(">$self->{'prefs'}");
  }
  if (! $prefs) {
      die "Couldn't create $self->{'prefs'}";
  }

  print $prefs "$self->{'pcid'}\n";
  print $prefs "$self->{'syncTime'}\n";
  $prefs = undef;

  if ($self->{'last status msg'}) {
      print STDOUT "\n";
  }
}

sub output {
  my $self = shift;
  my $str  = shift;

  if (!$self->{'last status msg'}) {
      print STDOUT ($self->{'prefix'} . $str);
      return;
  }



  if ($str !~ m/\n$/) {
      my $len = (length(": %") +
		 length($self->{'last status msg'}) +
		 length($self->{'last status percent'})) - length($str);
      print STDOUT ("\r" . $str . (" "x$len));
      return;
  }

  chop($str); # Drop the newline
  my $len = (length(": %") +
	     length($self->{'last status msg'}) +
	     length($self->{'last status percent'})) - length($str);

  my $msg = $self->{'last status msg'};
  my $done = $self->{'last status percent'};

  my $prevBar = $|;
  $| = 1;
  print STDOUT ("\r" . $str . (" "x$len) . "\n" .
		"$msg: $done%");
  $|=$prevBar;
}

sub status {
    my $self = shift;
    my $msg  = shift;
    my $done = shift;

    my $ctime = time;
    # Update at the earliest of 1 sec, 5 percent or new message...
    if (($self->{'last status msg'}      ne $msg)   ||
	($self->{'last status percent'}+5 < $done)  ||
	($self->{'last status time'}+1    < $ctime)) {
      # make sure the connection to palm doesn't time out...
      $self->update();

      my $pad = (" " x 
		 (length($self->{'last status msg'})    -length($msg)+
		  length($self->{'last status percent'})-length($done)));

      my $prevBar = $|;
      $| = 1;
      print STDOUT "\r$msg: $done%" . $pad;
      print STDOUT "\r$msg: $done%";
      $|=$prevBar;

      $self->{'last status msg'}     = $msg;
      $self->{'last status percent'} = $done;
      $self->{'last status time'}    = $ctime;
    }
}

sub getUserID {
  my $self = shift;
  return $self->{'info'}->{'userID'};
}

sub getUserName {
  my $self = shift;
  return $self->{'info'}->{'name'};
}

sub getConduitDir {
  my $self = shift;
  return $self->{'conduitdir'};
}

sub update {
  my $self = shift;
  $self->{'dlp'}->tickle();
}

1;
