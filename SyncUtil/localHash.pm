#------------------------------------------------------------------------------
#   $Date: 2001/11/04 13:17:57 $
#   RCS: $Id: localHash.pm,v 1.1.1.1 2001/11/04 13:17:57 tdeweese Exp $
#------------------------------------------------------------------------------

package localHash;

use strict;
use FileHandle;

sub new {
  my $type = shift;
  my $self =	{};
  
  bless ($self, $type);
  $self->{'host'}     = shift;
  $self->{'hashFile'} = shift;
  $self->{'syncTime'} = undef;
  $self->{'ok'}       = 0;

  $self->{'last-time'}    = 0;

  if (!$self->{'hashFile'}) {
    $self->{'hashFile'} = ($self->{'host'}->getUserName() . "_" .
			   $self->{'host'}->getUserID());
    $self->{'hashFile'} =~ s/\s+/_/g;
    $self->{'hashFile'} = ($self->{'host'}->getConduitDir() . "/ids." . 
			   $self->{'hashFile'});
			   
    $self->{'host'}->output($self->{'hashFile'} . "\n");
  }

  $self->readHash();
  $self;
}

sub finish {
  my $self = shift;
  $self->syncDate($self->{'host'}->{'info'}->{'thisSyncDate'});
  $self->writeHash();
}

sub readHash{
  my $self=shift;
  
  ## Load Pilot Hash's this lets us know if a pilot entry is new or
  ## if the record was deleted from BBDB.
  my $hashFile = new FileHandle($self->{'hashFile'});
  
  if ($hashFile)
    {
      $self->{'ok'} = 1;

      my $first = 1;
      while (<$hashFile>) {
	my ($id, $local_hash, $pilot_hash) = split(" ",$_);
	if ($first) {
	    $first = 0;
	    if ((!defined($local_hash) || !$local_hash) && 
		(!defined($pilot_hash) || !$pilot_hash)) {
		$self->syncDate($id);
		next;
	    } else {
		# Don't use 0 since PilotMgr uses that for forcing a sync
		$self->syncDate(1234);
	    }
	}
	$local_hash = undef if ($local_hash =~ m/<undef>/);
	$pilot_hash = undef if ($pilot_hash =~ m/<undef>/);

	$self->{'ids'}{$id} = [$local_hash, $pilot_hash];

	$self->update();
      }
      undef $hashFile;
    }
}

sub writeHash {
  my $self = shift;

  my $hashFile = new FileHandle(">$self->{'hashFile'}");
  if ($hashFile) 
  {
      if (defined $self->syncDate()) {
	  print $hashFile $self->syncDate() . "\n";
      }

      foreach my $id (keys(%{$self->{'ids'}})) {
	my ($local_hash, $pilot_hash) = @{$self->{'ids'}{$id}};
	next if ((!defined $local_hash) && (!defined $pilot_hash));
	$local_hash = "<undef>" if (!defined $local_hash);
	$pilot_hash = "<undef>" if (!defined $pilot_hash);
	print $hashFile $id . " " . $local_hash . " " . $pilot_hash . "\n";
      }

      undef $hashFile;
    } else {
      warn ("Unable to write Pilot ID's file: $self->{'hashFile'}\n"  .
	    "This will really screw up your next sync!!!\n" .
	    "It will think that all of your records on the pilot are new!");
    }
}

sub getNumIds {
    my $self = shift;
    return scalar(keys %{$self->{'ids'}});
}

sub getPilotHash {
  my $self = shift;
  my $id   = shift;
  return undef if (! defined $self->{'ids'}{$id});
  my ($local_hash, $pilot_hash) = @{$self->{'ids'}{$id}};
  return $pilot_hash;
}

sub getLocalHash {
  my $self = shift;
  my $id   = shift;
  return undef if (! defined $self->{'ids'}{$id});
  my ($local_hash, $pilot_hash) = @{$self->{'ids'}{$id}};
  return $local_hash;
}

sub setPilotHash {
  my $self = shift;
  my $id  = shift;
  my $val  = shift;
  my $local_hash = $self->getLocalHash($id);

  if ((!defined $local_hash) && (!defined $val)) {
    delete $self->{'ids'}{$id};
  } else {
    $self->{'ids'}{$id} = [$local_hash, $val];
  }
}

sub setLocalHash {
  my $self = shift;
  my $id  = shift;
  my $val  = shift;
  my $pilot_hash = $self->getPilotHash($id);

  if ((!defined $pilot_hash) && (!defined $val)) {
    delete $self->{'ids'}{$id};
  } else {
    $self->{'ids'}{$id} = [$val, $pilot_hash];
  }
}

sub syncDate {
  my $self    = shift;
  my $newDate = shift;

  if (defined $newDate) {
    $self->{'syncTime'} = $newDate;
  }

  return $self->{'syncTime'}
}

sub update {
  my $self = shift;
  my $ctime = time();
  # at least once a second give host some time...
  if ($self->{'last-time'} != $ctime)
    {
      $self->{'host'}->update();
      $self->{'last-time'} = $ctime;
    }
}

1;
