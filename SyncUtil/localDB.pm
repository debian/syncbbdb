#------------------------------------------------------------------------------
#   $Date: 2001/11/04 13:17:57 $
#   RCS: $Id: localDB.pm,v 1.1.1.1 2001/11/04 13:17:57 tdeweese Exp $
#------------------------------------------------------------------------------

package localDB;

use strict;

sub new {
  my $type = shift;
  my $self = {};
  bless ($self, $type);

  $self->{'host'} = shift;
  $self->{'last-time'} = 0;

  $self->{'fast'}    = "";
  $self->{'pilotDB'} = "";

  return $self;
}

sub getHost {
  my $self = shift;
  return $self->{'host'};
}

sub update {
  my $self = shift;
  my $ctime = time();
  # at least once a second give host some time...
  if ($self->{'last-time'} != $ctime)
    {
      $self->{'host'}->update();
      $self->{'last-time'} = $ctime;
    }
}

##############################################################################
###
###  This section contains methods that all subclasses MUST implement.
###
##############################################################################


sub getNew {
  my $self = shift;
  die ("Subclass must implement getNew to return a list of records that\n" .
       "are new in this database. Often this simply returns a list\n" .
       "constructed in setup.");
}

sub getMod {
  my $self = shift;
  die ("Subclass must implement getMod to return a list of records that\n" .
       "have been modified in this database. Often this simply returns a\n" .
       "list constructed in setup.");
}

sub getDel {
  my $self = shift;

  die ("Subclass must implement getDel to return a list of records that\n" .
       "have been delete from this database. Often this simply returns a\n" .
       "list constructed in setup.");
}

sub getRec {
  my $self = shift;
  my $id   = shift;
  die ("Subclass must implement this method to return the record with\n" .
       "the Pilot id requested or undef if not available.");
}

sub getPilotIDs {
  my $self = shift;
  my $rec  = shift;

  die ("Subclass must implement getPilotInfo to return the list of pilot-ids" .
       " associated with this record.");
}

sub dupRec {
  my $self = shift;
  my $rec  = shift;

  die ("Subclass must implement dupRec to copy $rec and return the\n"    .
       "copy.  Care must be taken to clear the associated pilot-id in\n" .
       "the new record");
}

sub syncRec {
  my $self      = shift;
  my $other_rec = shift;
  my $this_rec = shift;
  
  die ("Subclass must implement syncRec to merge contents of other_rec\n".
       "into this_rec (may be undef in which case create a new record).");
}

sub lastSyncDate {
  my $self = shift;

  # Example from lodalHashDB:
  # return $self->{'hashObj'}->syncDate();
  die ("Subclass must implement lastSyncDate to return the 'thisSyncDate'\n".
       "From the last sync that you think succeeded.\n");
}

##############################################################################
###
###  This section contains methods that many subclasses may want to 
###  override.
###
##############################################################################


sub name {
  my $self = shift;
  return "local";
}

sub output {
  my $self = shift;
  my $rec  = shift;

  ## Subclasses should really implement this to output a human
  ## readable one line description of rec.

  ## output the pilot record id if nothing else...
  $self->getHost()->output(join(", ", @{$self->getPilotIDs($rec)}) . "\n");
}

##############################################################################
###
###  This section contains methods that many subclasses may want to
###  augment. You need to call the base classes version of these
###  methods as well. Example:
###
###   $self->localDB::setup($fast, $pilotDB).
###
##############################################################################

sub setup {
  my $self = shift;

  $self->{'fast'}    = shift;
  $self->{'pilotDB'} = shift;
}

sub finish {
  my $self = shift;
  # Don't have anything to do but subclasses might override this
}


1;


