prefix=/usr/local
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
libdir=$(exec_prefix)/share
pdir=$(libdir)/perl5
pmdir=$(pdir)/PilotMgr

pfiles=SyncBBDB.pm
perldirs=SyncBBDB SyncUtil bbdb
pfiles1=localBBDB.pm pilotBBDBMap.pm pilotBBDB.pm pilotBBDBRecMap.pm	\
	pilotBBDBFieldMap.pm pilotBBDBTranslate.pm
pfiles2=StandAloneHost.pm localHash.pm pilotHashDB.pm ConduitHost.pm	\
	SyncEngine.pm localHashDB.pm PilotMgrHost.pm localDB.pm pilotDB.pm
pfiles3=bbdb.pm  bbdbAddr.pm  bbdbPhone.pm  bbdbRecord.pm

INSTALL=install
INSTALL_PROGRAM=$(INSTALL)
INSTALL_DATA=$(INSTALL) --mode 644

all:

install:
	-$(INSTALL) --directory $(DESTDIR)$(bindir)

#	$(INSTALL_PROGRAM) test $(DESTDIR)$(bindir)/syncbbdb-test
	$(INSTALL_PROGRAM) bootstrap $(DESTDIR)$(bindir)/syncbbdb-bootstrap

	-$(INSTALL) --directory $(DESTDIR)$(pmdir)
	-$(INSTALL) --directory $(DESTDIR)$(pdir)/SyncBBDB
	-$(INSTALL) --directory $(DESTDIR)$(pdir)/SyncUtil
	-$(INSTALL) --directory $(DESTDIR)$(pdir)/bbdb

	$(INSTALL_DATA) $(pfiles) $(DESTDIR)$(pmdir)/
	$(INSTALL_DATA) $(addprefix SyncBBDB/, $(pfiles1)) $(DESTDIR)$(pdir)/SyncBBDB
	$(INSTALL_DATA) $(addprefix SyncUtil/, $(pfiles2)) $(DESTDIR)$(pdir)/SyncUtil
	$(INSTALL_DATA) $(addprefix bbdb/,     $(pfiles3)) $(DESTDIR)$(pdir)/bbdb

clean:

.PHONY: all install clean
