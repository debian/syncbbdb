#------------------------------------------------------------------------------
#   $Date: 2001/11/04 13:17:56 $
#   RCS: $Id: pilotBBDBFieldMap.pm,v 1.1.1.1 2001/11/04 13:17:56 tdeweese Exp $
#------------------------------------------------------------------------------

package pilotBBDBFieldMap;
use strict;

sub new
{
    my $type = shift;
    my $self = {};

    bless($self, $type);

    $self->{'bbdb-type'}  = shift; # one of addr, phone, note
    $self->{'bbdb-name'}  = shift; # anything
    $self->{'pilot'}      = shift; # Field1-5, Custom1-4, Title, Note
    $self->{'pilot-label'}  = shift if (scalar @_); # for Fields 0-7 'label'
    
    return $self;
}

sub copy {
    my $self = shift;
    my $ret;
    if (defined $self->{'pilot-label'}) {
	return new pilotBBDBFieldMap($self->{'bbdb-type'},
				     $self->{'bbdb-name'},
				     $self->{'pilot'},
				     $self->{'pilot-label'});
    }

    return new pilotBBDBFieldMap($self->{'bbdb-type'},
				 $self->{'bbdb-name'},
				 $self->{'pilot'});
}

sub getBBDB {
    my $self = shift;
    return $self->{'bbdb-type'} . ":" . $self->{'bbdb-name'};
}

sub getPilot {
    my $self = shift;
    if (defined $self->{'pilot-label'}) {
	return $self->{'pilot'} . ":" . $self->{'pilot-label'};
    } else {
	return $self->{'pilot'};
    }
}

sub getPilotType {
    my $self = shift;
    my $str = $self->{'pilot'};
    return "Title"  if ($str eq "Title");
    return "Note"   if ($str eq "Note");
    return "Field"  if ($str =~ m/Field/);
    return "Custom" if ($str =~ m/Custom/);
}

1;
