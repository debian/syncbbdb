#------------------------------------------------------------------------------
#   $Date: 2001/11/04 13:17:57 $
#   RCS: $Id: pilotBBDBRecMap.pm,v 1.1.1.1 2001/11/04 13:17:57 tdeweese Exp $
#------------------------------------------------------------------------------

package pilotBBDBRecMap;
use strict;

use SyncBBDB::pilotBBDBFieldMap;

sub new
{
    my $type = shift;
    my $self = {};

    $self->{'id'}   	= shift;
    $self->{'category'} = shift;
    $self->{'name'} 	= shift if (scalar @_);
    $self->{'mappings'} = [];

    bless($self, $type);

    return $self;
}

sub copy {
    my $self = shift;
    my $nid  = shift;
    
    my $ret = new pilotBBDBRecMap($nid || 0, $self->{'category'});
    $ret->{'name'} = $self->{'name'} if (defined $self->{'name'});
    foreach my $ent (@{$self->{'mappings'}}) {
	push(@{$ret->{'mappings'}}, $ent->copy());
    }

    return $ret;
}

sub isFieldFree {
    my $self  = shift;
    my $field = shift;

    foreach my $ent (@{$self->{'mappings'}}) {
	return "" if ($ent->{'pilot'} eq $field);
    }
    return 1;
}

sub firstFree {
    my $self  = shift;
    my $pbbdb = shift;
    my $type  = shift;
    my $pname  = shift;

    my $ret = undef;
    if (($type eq "Title") || ($type eq "Note")) {
	return $type if ($self->isFieldFree($type));
    } 
    elsif ($type eq "Field") {
	foreach my $i (1..5) {
	    if ($self->isFieldFree("Field".$i)) {
		if (defined $pbbdb->{'phoneLabelsHash'}{$pname}) {
		    return ($type.$i, $pbbdb->{'phoneLabelsHash'}{$pname});
		} else {
		    return ($type.$i, undef);
		}
	    }
	}
    } elsif (($type eq "Custom") && (defined $pname)) {
	# print ("Lables: '" . 
	#        join("', '", keys %{$pbbdb->{'labelsHash'}}).
	#        "'\n");

	if (defined $pbbdb->{'labelsHash'}{$pname}) {
	    my $num = $pbbdb->{'labelsHash'}{$pname};
	    my $field = $pbbdb->{'Fields'}[$num];
	    return $field if ($self->isFieldFree($field));
	}
    } elsif ($type eq "Custom") {
	# Not given a name so we are just looking for an open custom field..
	# regardless of what it's label is...
	foreach my $i (1..4) {
	    return "Custom".$i if ($self->isFieldFree("Custom".$i));
	}
    }

    return undef;
}

sub leftFree {
    my $self  = shift;
    my $pbbdb = shift;
    my $num=0;
    foreach my $i (1..5) {
	$num++ if ($self->isFieldFree("Field".$i));
    }
    return $num;
}

sub addBBDB {
    my $self  = shift;
    my $pbbdb = shift;
    my $btype = shift;
    my $bname = shift;
    my $pname = shift;
    my $force = shift;

    my $ptype;

    if (defined $pbbdb->{'phoneLabelsHash'}{$pname}) {
	$ptype = "Field";
    } else {
	$ptype = "Custom";
    }

    if (($ptype eq "Field") || ($force && ($btype eq "phone"))) {
	my ($p,$pl) = $self->firstFree($pbbdb, "Field", $pname);

	if (defined $p) {
	    if (defined $pl) {
		push(@{$self->{'mappings'}}, 
		     new pilotBBDBFieldMap($btype, $bname, $p, $pl));
	    } elsif ($force) {
		push(@{$self->{'mappings'}}, 
		     new pilotBBDBFieldMap($btype, $bname, $p, -1));
	    }
	}
    } else {
	my $free = $self->firstFree($pbbdb, $ptype, $pname);
	if (defined $free) {
	    push(@{$self->{'mappings'}}, 
		 new pilotBBDBFieldMap($btype, $bname, $free));
	} elsif ($force) {
	    $free = $self->firstFree($pbbdb, $ptype);
	    push(@{$self->{'mappings'}}, 
		 new pilotBBDBFieldMap($btype, $bname, $free));
	}
    }
}

sub addMapping {
    my $self = shift;
    push(@{$self->{'mappings'}}, shift);
}

sub getUsedBBDBFields {
    my $self = shift;
    my %ret;

    foreach my $ent (@{$self->{'mappings'}}) {
	$ret{$ent->getBBDB()} = 1;
    }

    return \%ret;
}

sub removeBBDBMapping {
    my $self = shift;
    my $field = shift;
    my @result;
    foreach my $ent (@{$self->{'mappings'}}) {
	next if ($ent->getBBDB() eq $field);
	push(@result, $ent);
    }
    $self->{'mappings'} = \@result;
}

sub removePilotMapping {
    my $self = shift;
    my $field = shift;
    my @result;
    foreach my $ent (@{$self->{'mappings'}}) {
	next if ($ent->getPilot() eq $field);
	push(@result, $ent);
    }
    $self->{'mappings'} = \@result;
}

1;

