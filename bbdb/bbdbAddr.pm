#------------------------------------------------------------------------------
#   $Date: 2001/11/04 13:17:55 $
#   RCS: $Id: bbdbAddr.pm,v 1.1.1.1 2001/11/04 13:17:55 tdeweese Exp $
#------------------------------------------------------------------------------

package bbdbAddr;

use strict;

use bbdb::bbdb;

sub new {
    my $type = shift;
    my $self = {};
    my $str  = shift;
    my $ver  = shift || $bbdb::DEFAULT_VERSION;

    bless ($self, $type);

    $self->parse($str, $ver);
    $self;
}

sub parse
{
    my $self = shift;
    my $str = shift;
    my $ver = shift || $bbdb::DEFAULT_VERSION;

    return if ($str eq "nil");

    # ($name, $st1, $st2, $st3, $city, $state, $zip, $country) = 
    my @fields = bbdb::get_fields($str);

    my $idx = 0;
    $self->{'name'}  = $fields[$idx++];

    if ($ver gt 4) {
	# greater than ver 4 streets are a list
	$self->{'street'} = [];
	push(@{$self->{'street'}}, bbdb::get_fields($fields[$idx]))
	    if ($fields[$idx]);
	$idx++;
    } else {
	# Otherwise there are 3 street fields. We only include
	# them if they have text...
	$self->{'street'} = [ ];
	while ($idx lt 4) {
	    if ((defined $fields[$idx]) && $fields[$idx]) {
		push (@{$self->{'street'}}, $fields[$idx]);
	    }
	    $idx++;
	}
    }

    $self->{'city'}  = $fields[$idx++];
    $self->{'state'} = $fields[$idx++];

    my @zips = bbdb::get_fields($fields[$idx++]);
    if (scalar @zips) {
      $self->{'zip'}     = \@zips;
    }

    # Country was added in version 4
    if (($ver ge 4) && defined($fields[$idx])) {
	$self->{'country'}   = $fields[$idx];
    }
}

sub ePStr
{
    my $self = shift;
    my $ver  = shift || $bbdb::DEFAULT_VERSION;

    my $ret  = "[";
    $ret .= bbdb::eStr($self->{'name'}) . " ";

    # Addresses are a list under ver 4.

    if ($ver ge 5) {
	$ret .= bbdbRecord::eStrLst($self->{'street'}) . " ";
    } else { 
	my @streets = @{$self->{'street'}};

	while (scalar(@streets) lt 3) {
	    push(@streets, "");
	}
	while (scalar(@streets) gt 3) {
	    unpush(@streets);
	}

	foreach my $s (@streets) {
	    $ret .= bbdb::eStr($s) . " ";
	}
    }

    $ret .= bbdb::eStr($self->{'city'}) . " ";
    $ret .= bbdb::eStr($self->{'state'}) . " ";
    
    if ((defined $self->{'zip'}) and scalar(@{$self->{'zip'}})) {
	my @zips = @{$self->{'zip'}};
	if ($ver ge 6) {
	    # In version 6 zips are always one string! Yeah!
	    # So we just join up all the zips with spaces
	    # Don't worry about this too much since when going
	    # V6->V6 zips should normally just be a single element.
	    $ret .= "\"" . join(" ", @zips) . "\"";
	} elsif (scalar(@zips) == 1) {
	    if ($zips[0] =~ m/^\s*0+\s*$/) { # all zeros
		$ret .= "0";
	    } elsif ($zips[0] =~ m/\s*([0-9]+)\s*/){
		$ret .= $1;
	    } else {
		# Invalid zip!!! (one value not a plain number...)
		# So we tack an empty string on the end :(
		$ret .= "(\"" . $zips[0] . "\" \"\")";
	    }
	} else {
	    $ret .= "(\"" . join("\" \"", @zips) . "\")";
	}
    } else {
	if ($ver ge 6) {
	    # In version 6 zips are always one string! So in this
	    # case just write an empty string.
	    $ret .= "\"\"";
	} else {
	    $ret .= "0";
	}
    }

    # Country was added in version 4
    if ($ver ge 4) {
	$ret .= " " . bbdb::eStr($self->{'country'});
    }

    $ret .= "]";
    return $ret;
}

sub toString
{
  my $self = shift;
  my $ver  = shift || $bbdb::DEFAULT_VERSION;

  my $ret = "";

  my $len = length($self->{'name'}) + 2;
  my $fill = " " x $len;
  
  $ret .= $self->{'name'} . ": ";
  if (scalar @{$self->{'street'}}) {
      $ret .= join("\n$fill",  @{$self->{'street'}}) . "\n$fill";
  }

  if ($self->{'city'} || $self->{'state'} || $self->{'zip'}) 
    {
	if ($self->{'city'}) {
	    $ret .= $self->{'city'};
	} else { 
	    $ret .= "???";
	}
	
	if ($self->{'state'}) {
	    $ret .= ", " . $self->{'state'};
	} else {
	    $ret .= ", ??";
	}

	if ((defined $self->{'zip'}) and scalar(@{$self->{'zip'}})) {
	    $ret .= " " . join(" ", @{$self->{'zip'}});
	}

	# Country was added in version 4
	if ($ver ge 4) {
	    $ret .= " " . $self->{'country'};
	}

	$ret .= "\n";
    }

  return $ret;
}

sub print 
{
    my $self = shift;
    my $file = shift || \*STDOUT;

    print $file $self->toString();
}



sub eprint
{
    my $self = shift;
    my $ver  = shift;
    my $file = shift || \*STDOUT;

    print $file $self->ePStr($ver);
}
1;
